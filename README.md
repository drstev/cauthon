# Dovie'andi So Tovya Sagain
Cauthon is a roleplaying dice bot for Mumble, powered by
[`python-dice`](https://github.com/borntyping/python-dice) and
[`pymumble`](https://github.com/azlux/pymumble).
I needed a dice bot for a game I'm running with some folks in the Destination
Linux Network community.
All of the existing ones were either too simple for my use case or else so
out-of-date I couldn't get them to run on a modern \*nix system, so I decided
to write my own.

## Syntax
I promise I'll do a real write-up someday.
For now, you can refer to the
[`python-dice` docs](https://pypi.org/project/dice/).

## Installing
### Requirements
- Python 3, including `pip`
- `libopus` (for `pymumble`)

### Using `git`
- `git clone https://gitlab.com/TheRealDannyBoy/cauthon`
- `pip3 install --user -r requirements.txt` (or use a virtualenv if you prefer)
- Edit the variables near the top of `cauthon.py` to match the server you want
  to connect to
- Generate a certificate for Cauthon:
  ```
  openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -keyout botamusique.pem -out botamusique.pem -subj "/CN=botamusique"
  ```
- `python3 cauthon.py`

## Roadmap
- __Read configuration information from a file and/or the command line instead of
  from variables in the code itself.__
- __Create init scripts for FreeBSD.__  If you want the equivalent for your
  favorite OS, feel free to submit a merge request. 

## About the Name
The name refers to Mat Cauthon, a character from Robert Jordan's *Wheel of
Time* who has a strong affinity with gambling and luck.  The phrase at the top
of the README is his slogan, which translates to, "It's time to toss the dice."

## Credit where Credit Is Due 
The repo's avatar incorporates the DLN logo, created by Michael Tunnell, and "Dice" by Brent Newhall.
