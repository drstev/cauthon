import time
import os
from threading import Thread
import pymumble_py3
import pymumble_py3.constants
import dice
import dice.exceptions

# user variables
HOST = 'mumble.destinationlinux.network'
PORT = 64738
USER = 'cauthon'
CERTFILE = 'cauthon.pem'
PASSWORD = ''
CHANNEL = 'Gaming'
PIDFILE = 'cauthon.pid'


class Cauthon:
    def __init__(self):
        # store the process ID
        pid = str(os.getpid())
        file = open(PIDFILE, 'w')
        file.write("%s\n" % pid)
        file.close()

        self.last_roll = 'd6'  # initial seed value for the /reroll command
        self.mumble = pymumble_py3.Mumble(HOST, USER, port=PORT,
                                          certfile=CERTFILE, password=PASSWORD)
        self.mumble.callbacks.set_callback(
            pymumble_py3.constants.PYMUMBLE_CLBK_TEXTMESSAGERECEIVED,
            self.message_recieved)

        self.exit = False

        self.mumble.start()  # start the Mumble thread
        self.mumble.is_ready()  # wait for the end of the connection process

        self.game_channel = self.mumble.channels.find_by_name(CHANNEL)

        self.game_channel.move_in()  # move into the game channel
        self.mumble.users.myself.deafen()

        self.loop()

    # check recieved messages for die-rolling commands
    def message_recieved(self, text):
        message = text.message
        if message == '/q' or message == '/quit':
            self.exit = True
        elif message.startswith('/roll'):
            self.roll(message[5:].strip())
        elif message == '/reroll' or message == '/rr':
            self.roll(self.last_roll)
        # shorthand for /roll, put here to keep the reroll cases from being
        # eaten up
        elif message.startswith('/r'):
            self.roll(message[2:].strip())
        # special case for GURPS
        elif message.startswith('/g'):
            self.roll('3d6t' + message[2:].strip())
        # special case for FATE
        elif message.startswith('/f'):
            self.roll('4dft' + message[2:].strip())

    def roll(self, roll):
        try:
            self.game_channel.send_text_message(str(dice.roll(roll)))
            self.last_roll = roll
        except dice.exceptions.DiceBaseException:
            self.game_channel.send_text_message("There was an error parsing "
                                                "your dice string.")

    def loop(self):
        while not self.exit:
            time.sleep(1)

        os.remove(PIDFILE)


if __name__ == '__main__':
    cauthon = Cauthon()
